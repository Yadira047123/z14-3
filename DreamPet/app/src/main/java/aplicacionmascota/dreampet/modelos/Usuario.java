package aplicacionmascota.dreampet.modelos;

public class Usuario {
/*        db.execSQL("CREATE TABLE usuarios (\n" +
                "    id        INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "    tipdoc    VARCHAR (2)  NOT NULL,\n" +
                "    documento VARCHAR (11) NOT NULL,\n" +
                "    nombres   VARCHAR (50) NOT NULL,\n" +
                "    apellidos VARCHAR (50) NOT NULL,\n" +
                "    usuarios  VARCHAR (20) NOT NULL,\n" +
                "    clave     VARCHAR (80) NOT NULL\n" +
                ")");

 */

    private int id;
    private String tipdoc;
    private String documento;
    private String nombres;
    private String apellidos;
    private String usuarios;
    private String clave;

    public Usuario() {
    }

    public Usuario(String tipdoc, String documento, String nombres, String apellidos, String usuarios, String clave) {
        this.tipdoc = tipdoc;
        this.documento = documento;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.usuarios = usuarios;
        this.clave = clave;
    }

    public Usuario(int id, String tipdoc, String documento, String nombres, String apellidos, String usuarios, String clave) {
        this.id = id;
        this.tipdoc = tipdoc;
        this.documento = documento;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.usuarios = usuarios;
        this.clave = clave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipdoc() {
        return tipdoc;
    }

    public void setTipdoc(String tipdoc) {
        this.tipdoc = tipdoc;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(String usuarios) {
        this.usuarios = usuarios;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString(){
        String info = "/*** USUARIO ***/\n";
        info+= "Documento: " + this.tipdoc + " - " + this.documento + "\n";
        info+= "Nombre Completo: " + this.nombres + " " + this.apellidos + "\n";
        return info;
    }


    }

