package aplicacionmascota.dreampet.modelos;

public class Mascota {
/*        db.execSQL("CREATE TABLE `mascota` (\n" +
                "  `id` number primary key,\n" +
                "  `id` varchar(255) default NULL,\n" +
                "  `nombre` varchar(255) default NULL,\n" +
                "  `Estado` varchar(255) default NULL,\n" +
                "  `especie` varchar(255) default NULL,\n" +
                "  `fecha de nacimiento` varchar(255),\n" +
                "  `raza` varchar(255) default NULL,\n" +
                "  `direccion` varchar(255) default NULL,\n" +
                "  `coordenadas` varchar(30) default NULL,\n" +
                "  PRIMARY KEY (`id`)\n" +
                ")");

 */

    private int id;
    private String nombre;
    private String estado;
    private String especie;
    private String fecha_de_nacimiento;
    private String raza;
    private String direccion;
    private String coordenadas;

    public Mascota() {
    }

    public Mascota(String nombre, String estado, String especie, String fecha_de_nacimiento, String raza, String direccion, String coordenadas) {
        this.nombre = nombre;
        this.estado = estado;
        this.especie = especie;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.raza = raza;
        this.direccion = direccion;
        this.coordenadas = coordenadas;
    }

    public Mascota(int id, String nombre, String estado, String especie, String fecha_de_nacimiento, String raza, String direccion, String coordenadas) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.especie = especie;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.raza = raza;
        this.direccion = direccion;
        this.coordenadas = coordenadas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(String fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString(){
        String info = "/*** MASCOTA ***/\n";
        info+= "Nombre: " + this.nombre + "\n";
        info+= "Estado" + this.estado +"\n";
        return info;
    }


}
