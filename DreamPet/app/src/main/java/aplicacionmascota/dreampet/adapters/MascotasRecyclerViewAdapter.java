package aplicacionmascota.dreampet.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import java.util.ArrayList;

import aplicacionmascota.dreampet.R;
import aplicacionmascota.dreampet.modelos.Mascota;

public class MascotasRecyclerViewAdapter extends RecyclerView.Adapter<MascotasRecyclerViewAdapter.ViewHolderMascotas> {

    private ArrayList<Mascota> registros;

    public MascotasRecyclerViewAdapter(ArrayList<Mascota> registros) {
        this.registros = registros;
    }

    @NonNull
    @Override
    public MascotasRecyclerViewAdapter.ViewHolderMascotas onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.listadomascotas_viewholder, null, false);
        return new ViewHolderMascotas(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull MascotasRecyclerViewAdapter.ViewHolderMascotas holder, int position) {
        holder.cargarInformacion(registros.get(position));
    }

    @Override
    public int getItemCount() {
        return registros.size();
    }

    public class ViewHolderMascotas extends RecyclerView.ViewHolder {

        TextView nombre;
        TextView estado;
        TextView especie;
        TextView fecha_de_nacimiento;
        TextView raza;
        TextView direccion;


        public ViewHolderMascotas(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.listadomascotas_txtnombre);
            estado = itemView.findViewById(R.id.listadomascotas_txtestado);
            especie = itemView.findViewById(R.id.listadomascotas_especie);
            fecha_de_nacimiento = itemView.findViewById(R.id.listadomascotas_txtedad);
            raza = itemView.findViewById(R.id.listadomascotas_txtraza);
            direccion = itemView.findViewById(R.id.listadomascotas_txtdireccion);
        }

        public void cargarInformacion(Mascota registro)
        {
            nombre.setText(registro.getNombre());
            estado.setText(registro.getEstado());
            especie.setText(registro.getEspecie());
            fecha_de_nacimiento.setText(registro.getFecha_de_nacimiento());
            raza.setText(registro.getRaza());
            direccion.setText(registro.getDireccion());
        }
    }
}
