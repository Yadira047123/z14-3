package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import aplicacionmascota.dreampet.adapters.MascotasRecyclerViewAdapter;
import aplicacionmascota.dreampet.dao.MascotaDAO;
import aplicacionmascota.dreampet.modelos.Mascota;

public class ListadoMascotasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DreamPet);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_mascotas);

        RecyclerView recyclerMascotas = findViewById(R.id.listadoMascotas_listado);

        MascotaDAO baseDatos = new MascotaDAO(this);
        ArrayList<Mascota> resultadoObtenido = baseDatos.listar(null);

        MascotasRecyclerViewAdapter adaptadorMascota = new MascotasRecyclerViewAdapter(resultadoObtenido);
        recyclerMascotas.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerMascotas.setAdapter(adaptadorMascota);

        FloatingActionButton btaAgregarMascota = findViewById(R.id.listadoMascota_btaAgregar);
        btaAgregarMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), RegistrarMascotasActivity.class);
                startActivity(i);
            }
        });
    }
}