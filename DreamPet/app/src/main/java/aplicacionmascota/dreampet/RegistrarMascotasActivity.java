package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;

import aplicacionmascota.dreampet.dao.UsuarioDAO;
import aplicacionmascota.dreampet.dialogs.MensajeDialog;
import aplicacionmascota.dreampet.modelos.Usuario;

public class RegistrarMascotasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_mascotas);


        Button ibtEliminar = findViewById(R.id.registrarmascota_btneliminar);
        ibtEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MensajeDialog mensaje = new MensajeDialog();
                mensaje.show(getFragmentManager(),null);
/*
                Usuario registro = new Usuario ("CC", "1098643763", "Diana", "Afanador","daafanardor", "112345");
                System.out.println(registro.toString());

 */
            }
        });

        Button btnRegistrarNuevasMascotas = findViewById(R.id.registrarmascota_btnguardar);
        btnRegistrarNuevasMascotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               UsuarioDAO usuarioDao = new UsuarioDAO((v.getContext()));

                Usuario objeto = new Usuario(2,"CC", "103256546","laika","salamanca","laika88","12345");

     //           usuarioDao.insertar(objeto);

                ArrayList<Usuario> usuariosGuardados = usuarioDao.listar();

                usuarioDao.eliminar(objeto);

                usuariosGuardados = usuarioDao.listar();

            }
        });

        Button btnVolver = findViewById(R.id.registrarmascota_btnvolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}