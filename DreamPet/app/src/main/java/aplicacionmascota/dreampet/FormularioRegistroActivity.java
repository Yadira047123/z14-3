package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import aplicacionmascota.dreampet.dao.UsuarioDAO;
import aplicacionmascota.dreampet.modelos.Usuario;

public class FormularioRegistroActivity extends AppCompatActivity {

    private EditText txtTicDoc;
    private EditText txtDocumento;
    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtUsuario;
    private EditText txtClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_registro);

        Button btnGuardarDatos = findViewById(R.id.FormularioRegistro_btnGuardarDatos);
        btnGuardarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario modeloCargado = cargarModelo();
                if (modeloCargado!=null)
                {
                    UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());

                    if (baseDatos.insertar(modeloCargado)>0);
                        Toast.makeText(v.getContext(),"Se inserto el registro correctamente.",Toast.LENGTH_SHORT).show();
                    //else
                        Toast.makeText(v.getContext(),"Se ha produciodo un error al intentar insertar el registro.",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private Usuario cargarModelo()
    {
        Usuario modelo = null;

        String tipdoc = txtTicDoc.getText().toString();
        String documento = txtDocumento.getText().toString();
        String nombres = txtNombres.getText().toString();
        String apellidos = txtApellidos.getText().toString();
        String usuario = txtUsuario.getText().toString();
        String clave = txtClave.getText().toString();

        if (!tipdoc.isEmpty() && !documento.isEmpty() && !nombres.isEmpty() &&!apellidos.isEmpty() && !usuario.isEmpty() && !clave.isEmpty());
        {
            modelo = new Usuario(tipdoc,documento,nombres,apellidos,usuario,clave);

            return modelo;
        }
    }
}