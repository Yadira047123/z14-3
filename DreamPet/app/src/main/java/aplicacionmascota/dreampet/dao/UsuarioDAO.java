package aplicacionmascota.dreampet.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import aplicacionmascota.dreampet.clases.SqliteConex;
import aplicacionmascota.dreampet.modelos.Usuario;

public class UsuarioDAO {
    private Context contexto;
    private final String nombreTabla = "usuarios";
    private SqliteConex conexion;

    public UsuarioDAO(Context contexto) {
        this.contexto = contexto;
        this.conexion = new SqliteConex(this.contexto);
    }

    public long insertar (Usuario modeloUsuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();


        ContentValues valoresAInsertar = new ContentValues();
        valoresAInsertar.put( "tipdoc", modeloUsuario.getTipdoc());
        valoresAInsertar.put("documento",modeloUsuario.getDocumento());
        valoresAInsertar.put("nombres",modeloUsuario.getNombres());
        valoresAInsertar.put("apellidos",modeloUsuario.getApellidos());
        valoresAInsertar.put("dusuario",modeloUsuario.getUsuarios());
        valoresAInsertar.put("clave",modeloUsuario.getClave());


        return baseDatos.insert(this.nombreTabla,null,valoresAInsertar);
    }

    public ArrayList<Usuario>listar()
            //Se crean las variables y se inicializan, metodo lista
    {
        SQLiteDatabase baseDatos = this.conexion.getReadableDatabase();

        ArrayList<Usuario> registros = new ArrayList<Usuario>();

        // se determinan los campos a consultar
        String [] columnasAConsultar = {"id","tipdoc","documento", "nombres","apellidos","usuario","clave"};

// se ejecuta la consulta y se almacena el cursor
        Cursor cursor = baseDatos.query(this.nombreTabla, columnasAConsultar,null,null,null,null,null);
//se verifica que la consulta haya sido satisfactoria
        if (cursor!=null)
        {
            //Se posiciona en el primer itam de la tabla el cursor
           cursor.moveToFirst();

           do{
               /*
               public Usuario(int id, String tipdoc, String documento, String nombres, String apellidos, String usuarios, String clave)
                */
               Usuario registro = new Usuario(
                       cursor.getInt(0),
                       cursor.getString(1),
                       cursor.getString(2),
                       cursor.getString(3),
                       cursor.getString(4),
                       cursor.getString(5),
                       cursor.getString(6)

               );
               registros.add(registro);
           }while (cursor.moveToNext());

           cursor.close();
        }
        return registros;
    }

    public int actualizar (Usuario modeloUsuario){

        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        ContentValues valoresAActualizar = new ContentValues();
        valoresAActualizar.put( "tipdoc", modeloUsuario.getTipdoc());
        valoresAActualizar.put("documento",modeloUsuario.getDocumento());
        valoresAActualizar.put("nombres",modeloUsuario.getNombres());
        valoresAActualizar.put("apellidos",modeloUsuario.getApellidos());
        valoresAActualizar.put("dusuario",modeloUsuario.getUsuarios());
        valoresAActualizar.put("clave",modeloUsuario.getClave());

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloUsuario.getId()) };


        return baseDatos.update(this.nombreTabla,valoresAActualizar, criterioWhere, valoresCriterioWhere);
    }

    public int eliminar(Usuario modeloUsuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloUsuario.getId())};

       return baseDatos.delete(this.nombreTabla,criterioWhere,valoresCriterioWhere);
    }
}
